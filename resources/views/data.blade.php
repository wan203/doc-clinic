@extends('layouts.app')
@section('content')

    <h2 style="text-align: center">ระบบการจองคิวพบสัตวแพทย์ออนไลน์</h2>
    <form method="post" action="{{url('/tableDog')}}" >
        @csrf

        <div class="continue" style="text-align: center; margin-top: 40px;">
            <button type="button" class="btn btn-light" style="margin-right: 40px;">จองคิวออนไลน์</button>
            <button type="button" class="btn btn-light"style="margin-right: 40px;">ตารางนัดสัตวแพทย์</button>
            <button type="button" class="btn btn-warning"style="margin-right: 40px;">ดูข้อมูลทั้งหมด</button>
            <button type="button" class="btn btn-light">ดูสถิติการจอง</button>
        </div>

        <div class="container">
            <input class="form-control" id="myInput" type="text" placeholder="ค้นหา"
                   style="width: 200px;margin-left: 910px;margin-top: 30px;">
            <br>
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th>ลำดับ</th>
                    <th>ชื่อสุนัข</th>
                    <th>ชื่อผู้จอง</th>
                    <th>เบอร์โทร</th>
                    <th>วันที่</th>
                    <th>เวลา</th>
                    <th>อาการ</th>


                </tr>
                </thead>
                <tbody id="myTable">
                <tr>
                    <td>1</td>
                    <td>ผักขม</td>
                    <td>วนิดา เฉลิมพิศ</td>
                    <td>0984282224</td>
                    <td>22/05/2020</td>
                    <td>11:00</td>
                    <td>เจ็บขา</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>โกโก้</td>
                    <td>วนิดา เฉลิมพิศ</td>
                    <td>0984282224</td>
                    <td>23/05/2020</td>
                    <td>12:00</td>
                    <td>แผลที่ใบหู</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>แซนวิส</td>
                    <td>สมศักดิ์ อู่ทอง</td>
                    <td>0886754532</td>
                    <td>19/05/2020</td>
                    <td>09:00</td>
                    <td>ตรวจสุขภาพ</td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>ผักขม</td>
                    <td>วนิดา เฉลิมพิศ</td>
                    <td>0984282224</td>
                    <td>10/05/2020</td>
                    <td>12:00</td>
                    <td>ตรวจสุขภาพ</td>
                </tr>
                <tr>
                    <td>5</td>
                    <td>แซนวิส</td>
                    <td>สมศักดิ์ อู่ทอง</td>
                    <td>0886754532</td>
                    <td>10/05/2020</td>
                    <td>13:00</td>
                    <td>ตรวจสุขภาพ</td>
                </tr>
                </tbody>
            </table>

        </div>


        <script>
            $(document).ready(function(){
                $("#myInput").on("keyup", function() {
                    var value = $(this).val().toLowerCase();
                    $("#myTable tr").filter(function() {
                        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                    });
                });
            });
        </script>

    </form>
@endsection
