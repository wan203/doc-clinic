@extends('layouts.app')
@section('content')

    <h2 style="text-align: center">ระบบการจองคิวพบสัตวแพทย์ออนไลน์</h2>
    <form method="post" action="{{url('/tableDog')}}" >
        @csrf

        <div class="continue" style="text-align: center; margin-top: 40px;">
            <button type="button" class="btn btn-light" style="margin-right: 40px;">จองคิวออนไลน์</button>
            <button type="button" class="btn btn-light"style="margin-right: 40px;">ตารางนัดสัตวแพทย์</button>
            <button type="button" class="btn btn-light"style="margin-right: 40px;">ดูข้อมูลทั้งหมด</button>
            <button type="button" class="btn btn-warning">ดูสถิติการจอง</button>
        </div>

        <label style="text-align: center; margin-top: 50px;margin-left: 900px;">การจองคิวทั้งหมด ..... ครั้ง</label>


        <div class="container" style="margin-top: 30px;">

            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th>ลำดับ</th>
                    <th>ชื่อผู้จอง</th>
                    <th>จำนวนครั้ง</th>
                </tr>
                </thead>

                <tbody >
                <tr>
                    <td>1</td>
                    <td>วนิดา เฉลิมพิศ</td>
                    <td>4</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>สมศักดิ์ อู่ทอง</td>
                    <td>1</td>
                </tr>

                </tbody>
            </table>
        </div>

    </form>
@endsection
