@extends('layouts.app')
@section('content')

        <h2 style="text-align: center;">ระบบการจองคิวพบสัตวแพทย์ออนไลน์</h2>
    <form method="post" action="{{url('/')}}" >
        @csrf

        <div class="continue" style="text-align: center; margin-top: 40px;">
          <button type="button" class="btn btn-warning" style="margin-right: 40px;">จองคิวออนไลน์</button>
          <button type="button" class="btn btn-light"style="margin-right: 40px;">ตารางนัดสัตวแพทย์</button>
          <button type="button" class="btn btn-light"style="margin-right: 40px;">ดูข้อมูลทั้งหมด</button>
          <button type="button" class="btn btn-light">ดูสถิติการจอง</button>
        </div>

        <div class="form-group"style="margin-right: 300px;margin-left: 300px;margin-top: 50px;">
            <label>เลือกวันที่</label>
            <input type="date" name ="date" placeholder="date" class="form-control" >

            <label>เลือกเวลา</label>
            <input type="time" name ="time" placeholder="time" class="form-control" >

            <label>ชื่อ-นามสกุล</label>
            <input type="text" name ="name"  class="form-control" >

            <label>ชื่อสุนัข</label>
            <input type="text" name ="dog"  class="form-control" >

            <label>เบอร์โทร</label>
            <input type="tel" name ="phone"  class="form-control" >

            <label>อาการเบื้องต้น</label>
            <input type="text" name ="symptom"  class="form-control" >

        </div>
        <button type="submit" class="btn btn-warning" style="margin-top: 10px; margin-left: 700px;margin-bottom: 50px;
            padding-left: 30px;padding-right: 30px;">จองคิว</button>
    </form>
@endsection
